using System;
using System.Collections.Generic;
using BusinessLayer.Extensions;
using BusinessLayer.Models;
using Xunit;

namespace Test;

public class WatchUnitTest
{
    [Fact]
    public void Test_GivenNewWatch_WhenGetTime_ThenReturnsDefaultHour()
    {
        // Given
        Watch watch = new Watch("Rollex", WatchType.Digital);
        const string expectedTime = "il est 10 h, 10 mn et 0 s";

        // When
        var actualTime = watch.GetTime();

        watch.ThrowWatch(10);



        // Then
        Assert.Equal(expectedTime, actualTime);
        Assert.Equal(50, actualTime.Length);
        Assert.Contains("il est", actualTime);
    }
}