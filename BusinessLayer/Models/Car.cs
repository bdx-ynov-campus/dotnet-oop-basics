namespace BusinessLayer.Models
{
    public class Car
    {
        public string Brand { get; set; }

        public int MyProperty { get; set; }

        private string _vin;

        public Car(string brand)
        {
            Brand = brand;
            StartEngine();
        }
        protected void StartEngine()
        {
            Console.WriteLine("Coucou");
        }
    }

    public class BlueCar : Car
    {
        public BlueCar(string brand) : base(brand)
        {

        }

        public void OpenDoors()
        {
            StartEngine();
        }
    }

}