namespace BusinessLayer.Models
{
    public class Watch
    {
        public string Brand { get; }
        public WatchType Type { get; }
        private int _hours;
        private int _minutes;
        private int _seconds;

        public Watch(string brand, WatchType type)
        {
            Brand = brand;
            Type = type;
            _hours = 10;
            _minutes = 10;
            _seconds = 0;
        }

        public string GetTime()
        {
            if (Type == WatchType.Digital)
            {
                return $"il est {_hours}:{_minutes}:{_seconds}";
            }

            return $"il est {_hours} h, {_minutes} mn et {_seconds} s";
        }

        public void SetTime(int hours, int minutes, int seconds)
        {
            _hours = hours;
            _minutes = minutes;
            _seconds = seconds;
            Console.WriteLine("J'ai bien réglé tout");
        }

        public void SetTime(int hours)
        {

            _hours = hours;

        }

        public void SetTime(int hours, int minutes){}

        
    }
}